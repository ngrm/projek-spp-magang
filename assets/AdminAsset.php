<?php
/**
 * @link https://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license https://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "assets/css/animate.css",
        "assets/fonts/bootstrap/bootstrap-icons.css",
        "assets/css/main.min.css",
        "assets/vendor/overlay-scroll/OverlayScrollbars.min.css",
        "assets/vendor/tagsCloud/tagsCloud.css"
        // 'css/site.css',
        // 'lumia/css/style.css',
    ];
    public $js = [
        "assets/js/jquery.min.js",
		"assets/js/bootstrap.bundle.min.js",
		"assets/js/modernizr.js",
		"assets/js/moment.js",
        "assets/vendor/overlay-scroll/jquery.overlayScrollbars.min.js",
		"assets/vendor/overlay-scroll/custom-scrollbar.js",
        "assets/vendor/apex/apexcharts.min.js",
		"assets/vendor/apex/custom/ecommerce/orders-visits.js",
		"assets/vendor/apex/custom/ecommerce/visitors.js",
		"assets/vendor/apex/custom/ecommerce/customers.js",
        "assets/vendor/tagsCloud/tagsCloud.js",
        "assets/vendor/jvectormap/jquery-jvectormap-2.0.5.min.js",
		"assets/vendor/jvectormap/world-mill-en.js",
		"assets/vendor/jvectormap/gdp-data.js",
		"assets/vendor/jvectormap/custom/world-map-markers2.js",
        "assets/js/main.js"

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap5\BootstrapAsset'
    ];
}
