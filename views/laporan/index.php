<?php

use app\models\Laporan;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\LaporanSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Laporan Tunggakan Spp';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="laporan-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Laporan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'nisn',
            'nama',
            'kelas',
            'jumlah_tunggakan',
            'nominal_tunggakan',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Laporan $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'nisn' => $model->nisn]);
                 }
            ],
        ],
    ]); ?>


</div>