<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Laporan $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="laporan-form">

    <?php $form = ActiveForm::begin([
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-sm-8\">{input}</div>\n<div class=\"col-sm-4\">{error}</div>",
            'labelOptions' => ['class' => 'col-sm-4 control-label'],
        ],
    ]); ?>

    <?= $form->field($model, 'nisn')->dropDownList(
        \yii\helpers\ArrayHelper::map(\app\models\Siswa::find()->all(), 'nisn', 'nisn'),
        ['prompt' => 'Pilih Nomor NISN']
    ) ?>

    <?= $form->field($model, 'nama')->dropDownList(
        \yii\helpers\ArrayHelper::map(\app\models\Siswa::find()->all(), 'nama', 'nama'),
        ['prompt' => 'Pilih nama siswa']
    ) ?>

    <?= $form->field($model, 'kelas')->dropDownList(
        \yii\helpers\ArrayHelper::map(\app\models\Kelas::find()->all(), 'nama_kelas', 'nama_kelas'),
        ['prompt' => 'Pilih nama kelas']
    ) ?>

    <?= $form->field($model, 'jumlah_tunggakan', ['inputOptions' => ['class' => 'form-control']]) ?>

    <?= $form->field($model, 'nominal_tunggakan', ['inputOptions' => ['class' => 'form-control']]) ?>

    <div class="form-group">
        <div class="col-sm-offset-4 col-sm-8">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>

</div>
