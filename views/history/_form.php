<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\History $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="history-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'id_kelas')->textInput() ?>

    <?= $form->field($model, 'id_spp')->textInput() ?>

    <?= $form->field($model, 'id_petugas')->textInput() ?>

    <?= $form->field($model, 'nominal')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jumlah_dibayar')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tanggal_dibayar')->textInput() ?>

    <?= $form->field($model, 'keterangan')->dropDownList([ 'LUNAS' => 'LUNAS', 'BELUM LUNAS' => 'BELUM LUNAS', ], ['prompt' => '']) ?>

    <div class="form-group" style="margin-top: 20px;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
