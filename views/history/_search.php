<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\HistorySearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="history-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'nisn') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'id_kelas') ?>

    <?= $form->field($model, 'id_spp') ?>

    <?= $form->field($model, 'id_petugas') ?>

    <?php // echo $form->field($model, 'nominal') ?>

    <?php // echo $form->field($model, 'jumlah_dibayar') ?>

    <?php // echo $form->field($model, 'tanggal_dibayar') ?>

    <?php // echo $form->field($model, 'keterangan') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
