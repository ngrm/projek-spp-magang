<?php
$host = "localhost";
$user = "root";
$password = "";
$db_nama = "pembayaran_spp";
$port = 3306;

// membuat koneksi ke database MySQL
$conn = mysqli_connect($host, $user, $password, $db_nama, $port);

// memeriksa koneksi
if (!$conn) {
    die("Koneksi gagal: " . mysqli_connect_error());
}