<?php

/**   @var yii\web\View $this */
/**   @var yii\bootstrap5\ActiveForm $form */

/** @var app\models\LoginForm $model */

use yii\bootstrap5\ActiveForm;
use yii\bootstrap5\Html;


$this->title = 'Sign In';
// $this->params['breadcrumbs'][] = $this->title;


?>

<style> 

    body {
        background-image: url('https://images.unsplash.com/photo-1542273917363-3b1817f69a2d?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D&auto=format&fit=crop&w=2074&q=80');
    }
    .image-container i {
        color: black;
        font-size: 50px;
    }
    .custom-border {
        background-color: rgba(255, 255, 255, 0.5); 
        padding: 20px; 
        border-radius: 30px;
    }
</style>

<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

</head>
<body>
    <div class="site-login">
        <div class="image-container mb-5 text-center">
            <img src="" alt="">
    <!-- <i class="fa-solid fa-user"></i> -->
    </div>

    <h1 style="text-align:center"><?= Html::encode($this->title) ?></h1>
    <!-- <p style="text-align:center">Please fill out the following fields to login:</p> -->
    
    <div class="row justify-content-center">
        <div class="col-lg-4 custom-border mb-4 p-4">
            <?php $form = ActiveForm::begin([
                'id' => 'login-form',
                'fieldConfig' => [
                    'template' => "{label}\n{input}\n{error}",
                    'labelOptions' => ['class' => 'col-lg-1 col-form-label'],
                    'inputOptions' => ['class' => 'col-lg-3 form-control'],
                    'errorOptions' => ['class' => 'col-lg-7 invalid-feedback'],
                ],
            ]); ?>

            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

            <!-- ... -->
            <?= $form->field($model, 'password')->passwordInput(['id' => 'password-input']) ?>
            <button type="button" id="toggle-password" class="btn btn-light">
                <i class="fas fa-eye"></i>
            </button>
            <!-- ... -->
            <script>
                const passwordInput = document.getElementById('password-input');
                const togglePassword = document.getElementById('toggle-password');

                togglePassword.addEventListener('click', () => {
                    if (passwordInput.type === 'password') {
                        passwordInput.type = 'text';
                        togglePassword.innerHTML = '<i class="fas fa-eye-slash"></i>';
                    } else {
                        passwordInput.type = 'password';
                        togglePassword.innerHTML = '<i class="fas fa-eye"></i>';
                    }
                });
            </script>
            <!-- ... -->

            <?= $form->field($model, 'rememberMe')->checkbox([
                'template' => "<div class=\"custom-control custom-checkbox\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
            ]) ?>

            <div class="form-group">
                <div>
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary form-control', 'name' => 'login-button']) ?>
                </div>
            </div>

            <a href="<?= Yii::$app->urlManager->createUrl(['/registrasi/create']) ?>"><p class="text-center pt-2">Don't have an account? Please register now!</p></a>

            <style>
                a {
                    text-decoration: none;
                    font-size: 13px;
                }
            </style>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</body>