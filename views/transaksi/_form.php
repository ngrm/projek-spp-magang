<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Transaksi $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="transaksi-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_petugas')->dropDownList(
        \yii\helpers\ArrayHelper::map(\app\models\petugas::find()->all(), 'id_petugas', 'nama_petugas'),
        ['prompt' => 'Pilih ID Petugas']
    ) ?>

    <?= $form->field($model, 'nisn')->dropDownList(
            \yii\helpers\ArrayHelper::map(\app\models\Siswa::find()->all(), 'nisn', 'nisn'),
            ['prompt' => 'Pilih Nomor Nisn']
    ) ?>

    <?= $form->field($model, 'tagihan_siswa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'jumlah_bayar')->textInput(['maxlength' => true, 'placeholder' => 'Masukkan jumlah bayar']) ?>

    <?= $form->field($model, 'kekurangan')->textInput(['maxlength' =>  true, 'placeholder' => 'Masukkan jumlah kekurangan']) ?>

    <?= $form->field($model, 'tgl_bayar')->textInput(['type' => 'date']) ?>

    <?= $form->field($model, 'id_spp')->dropDownList(
        \yii\helpers\ArrayHelper::map(\app\models\Spp::find()->all(), 'id_spp', 'tahun'),
        ['prompt' => 'Pilih ID Spp']
    ) ?>

    <?= $form->field($model, 'status')->dropDownList(
    ['LUNAS' => 'LUNAS', 'BELUM LUNAS' => 'BELUM LUNAS'],
    ['prompt' => 'Pilih Status Transaksi']
    ) ?>


    <div class="form-group" style="margin-top: 20px;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
