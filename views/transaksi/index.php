<?php

use app\models\Transaksi;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\TransaksiSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Transaksi';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaksi-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Transaksi', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'tgl_bayar',
            'id_transaksi',
            'id_petugas',
            'nisn',
            'tagihan_siswa',
            'jumlah_bayar',
            // 'kekurangan',
            'id_spp',
            // 'status',
            [
                'attribute' => 'notifikasi',
                'format' => 'raw',
                'value' => function ($model) {
                    // Ganti nomor WhatsApp berikut dengan nomor yang ingin Anda gunakan
                    $nomorWhatsApp = '+6289672201098';
            
                    // Pesan yang ingin Anda kirim
                    $pesan = 'Konfirmasi Pembayaran :

                    Telah dilakukan pembayaran pada 0000-00-00 00:00
                    Nama : 
                    Biaya: SPP Bulan JANUARI (contoh)
                    Nominal : 
                    Dibayar :
                    
                    
                    TTD
                    Petugas SMK N 1 Ngepet
                    
                    *Harap tidak membalasan pesan ini';
            
                    // Encode pesan ke dalam format URL
                    $pesanEncoded = urlencode($pesan);
            
                    // Membuat link WhatsApp dengan nomor dan pesan yang sudah ditentukan
                    $linkWhatsApp = 'https://wa.me/' . $nomorWhatsApp . '?text=' . $pesanEncoded;
            
                    // Menggunakan ikon WhatsApp dari Font Awesome
                    $iconWhatsApp = '<i class="fab fa-whatsapp"></i>';
            
                    // Membuat tautan dengan ikon WhatsApp
                    return Html::a($iconWhatsApp, $linkWhatsApp, ['target' => '_blank', 'title' => 'Hubungi via WhatsApp']);
                },
            ],            
            
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Transaksi $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id_transaksi' => $model->id_transaksi]);
                 }
            ],
        ],
    ]); ?>


</div>
