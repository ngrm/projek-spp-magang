<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var yii\web\View $this */
/* @var app\models\Contact $model */
/* @var yii\widgets\ActiveForm $form */
?>

<div class="contact-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-5">
            <?= $form->field($model, 'name')->textInput([
                'maxlength' => true,
                'placeholder' => 'Enter Your Name'
            ]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'email')->textInput([
                'maxlength' => true,
                'placeholder' => 'Enter Your Email'
            ]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'subject')->textInput([
                'maxlength' => true,
                'placeholder' => 'Enter Subject'
            ]) ?>
        </div>
        <div class="col-md-5">
            <?= $form->field($model, 'body')->textarea([
                'rows' => 3,
                'placeholder' => 'Enter Your Message'
            ]) ?>
        </div>
        <div class="col-md-5 offset-md-5">
            <?= $form->field($model, 'agree')->checkbox([
                'template' => "{input}\n{label}",
            ]) ?>
        </div>
    </div>

    <div class="col-md-5 offset-md-5 text-right">
        <?= Html::submitButton('Send Message', ['class' => 'btn btn-dark form-control', 'style' => 'border-radius: 20px;']) ?>
    </div>

</div>

<style>
    .help-block {
        color: red;
        font-size: 13px;
    }

    .contact-form {
        margin-bottom: 100px;
    }
</style>

<?php ActiveForm::end(); ?>
