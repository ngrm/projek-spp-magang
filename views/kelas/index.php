<?php

use app\models\Kelas;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var app\models\KelasSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Kelas';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1><?= Html::encode($this->title) ?></h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header with-border">
                <?= Html::a('<i class="fa fa-plus"></i> Create', ['create'], ['class' => 'btn btn-success']) ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        'id_kelas',
                        'nama_kelas',
                        'kompotensi_keahlian',
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{view} {update} {delete}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a('<i class="fa fa-eye"></i>', $url, [
                                        'title' => 'View',
                                        'class' => 'btn btn-primary btn-xs',
                                    ]);
                                },
                                'update' => function ($url, $model, $key) {
                                    return Html::a('<i class="fa fa-pencil"></i>', $url, [
                                        'title' => 'Update',
                                        'class' => 'btn btn-warning btn-xs',
                                    ]);
                                },
                                'delete' => function ($url, $model, $key) {
                                    return Html::a('<i class="fa fa-trash"></i>', $url, [
                                        'title' => 'Delete',
                                        'class' => 'btn btn-danger btn-xs',
                                        'data-confirm' => 'Are you sure you want to delete this item?',
                                    ]);
                                },
                            ],
                            'urlCreator' => function ($action, Kelas $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'id_kelas' => $model->id_kelas]);
                            }
                        ],
                    ],
                ]); ?>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </section>
    <!-- /.content -->
</div>
