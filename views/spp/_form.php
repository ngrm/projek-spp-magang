<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\spp $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="spp-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tahun')->textInput() ?>

    <?= $form->field($model, 'nominal')->textInput() ?>

    <div class="form-group" style="margin-top: 20px;">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <style>
        .help-block {
        color: #FF0000; 
        }
    </style>
    
    <?php ActiveForm::end(); ?>

</div>
