`<?php

/** @var yii\web\View $this */
/** @var string $content */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

AppAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Modernize Free</title>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css">  
  <link rel="shortcut icon" type="image/png" href="src/assets/images/logos/favicon.png" />
  <link rel="stylesheet" href="src/assets/css/styles.min.css" />
   <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<body>
  <!--  Body Wrapper -->
   <div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
    data-sidebar-position="fixed" data-header-position="fixed">
          <!-- Sidebar -->
          <?php $this->beginContent("@app/views/layouts/admin/component/sidebar.php") ?>
          <?php $this->endContent() ?>
        <!-- End of Sidebar -->
    <!--  Main wrapper -->

    <div class="body-wrapper">
        <!-- Topbar -->
          <?php $this->beginContent("@app/views/layouts/admin/component/topbar.php") ?>
          <?php $this->endContent() ?>
        <!-- End of Topbar -->

      <div class="container-fluid">
        <!--  Row 1 -->
        <main id="main" class="flex-shrink-0" role="main">
            <div class="container">
                <?php if (!empty($this->params['breadcrumbs'])): ?>
                    <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
                <?php endif ?>
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </main>
                    
  <script src="src/assets/libs/jquery/dist/jquery.min.js"></script>
  <script src="src/assets/libs/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
  <script src="src/assets/js/sidebarmenu.js"></script>
  <script src="src/assets/js/app.min.js"></script>
  <script src="src/assets/libs/apexcharts/dist/apexcharts.min.js"></script>
  <script src="src/assets/libs/simplebar/dist/simplebar.js"></script>
  <script src="src/assets/js/dashboard.js"></script>
</body>
  <!-- Footer -->
  <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; payment application <?= date('Y') ?></span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->
</html>`
      <style>
              
                .help-block {
                    color: #FF0000; 
                }
                ::-webkit-scrollbar {
                    width: 0.0em; /* Lebar scrollbar */
                }

                ::-webkit-scrollbar-track {
                    background-color: transparent; /* Warna latar belakang track */
                }

                ::-webkit-scrollbar-thumb {
                    background-color: transparent; /* Warna thumb scrollbar */
                }

                * {
                    scrollbar-width: none; /* Untuk Firefox versi terbaru */
                }

                *::-ms-scrollbar {
                    width: 0.0em; /* Lebar scrollbar */
                }

                *::-ms-scrollbar-thumb {
                    background-color: transparent; /* Warna thumb scrollbar */
                }
                
                .page-wrapper.sidebar-open {
                    transform: translateX(260px); /* Sesuaikan dengan lebar sidebar Anda */
                    transition: transform 0.3s ease; /* Efek transisi saat menggeser */
                }

                @media (max-width: 991.98px) {
                    .page-wrapper.sidebar-open {
                        transform: translateX(0); /* Geser konten ke kanan saat sidebar terbuka di layar kecil */
                    }
                }

        </style>