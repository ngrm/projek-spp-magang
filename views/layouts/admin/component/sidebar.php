<!--  Body Wrapper -->
<div class="page-wrapper" id="main-wrapper" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
		data-sidebar-position="fixed" data-header-position="fixed">
		<!-- Sidebar Start -->
		<aside class="left-sidebar">
		<!-- Sidebar scroll-->
		<div>
			<div class="brand-logo d-flex align-items-center justify-content-between">
			<a href="src/index.html" class="text-nowrap logo-img">
				<img src="src/assets/images/logos/dark-logo.svg" width="180" alt="" />
			</a>
			<div class="close-btn d-xl-none d-block sidebartoggler cursor-pointer" id="sidebarCollapse">
				<i class="ti ti-x fs-8"></i>
			</div>
			</div>
			<!-- Sidebar navigation-->
			<nav class="sidebar-nav scroll-sidebar" data-simplebar="">
			<ul id="sidebarnav">
				<li class="nav-small-cap">
				<i class="ti ti-dots nav-small-cap-icon fs-4"></i>
				<span class="hide-menu">Menu</span>
				</li>
				<li class="sidebar-item">
          <a class="sidebar-link" href="<?= Yii::$app->urlManager->createUrl(['site/index']) ?>" aria-expanded="false">
              <span>
                  <i class="ti ti-layout-dashboard"></i>
              </span>
              <span class="hide-menu">Dashboard</span>
          </a>
      </li>
				</a>
				</li>
				<li class="sidebar-item">
				<a class="sidebar-link" href="http://localhost/trueprojek/web/index.php?r=siswa%2Findex" aria-expanded="false">
					<span>
					<i class="fa-solid fa-user"></i>
					</span>
					<span class="hide-menu">Siswa</span>
				</a>
				</li>
				<li class="sidebar-item">
				<a class="sidebar-link" href="http://localhost/trueprojek/web/index.php?r=kelas%2Findex" aria-expanded="false">
					<span>
					  <i class="fa-solid fa-school"></i>
					</span>
					<span class="hide-menu">Kelas</span>
				</a>
				</li>
				<li class="sidebar-item">
				<a class="sidebar-link" href="http://localhost/trueprojek/web/index.php?r=spp%2Findex" aria-expanded="false">
					<span>
					  <i class="fa-solid fa-coins"></i>
					</span>
					<span class="hide-menu">Spp</span>
				</a>
				</li>
				<li class="sidebar-item">
				<a class="sidebar-link" href="http://localhost/trueprojek/web/index.php?r=petugas%2Findex" aria-expanded="false">
					      <span>
                  <i class="fa-solid fa-person-circle-check"></i>
                </span>
                <span class="hide-menu">Petugas</span>
              </a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link" href="http://localhost/trueprojek/web/index.php?r=transaksi%2Findex" aria-expanded="false">
                <span>
                  <i class="fa-solid fa-money-check-dollar"></i>
                </span>
                <span class="hide-menu">Transaksi</span>
              </a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link" href="http://localhost/trueprojek/web/index.php?r=laporan%2Findex" aria-expanded="false">
                <span>
                  <i class="fa-solid fa-folder-open"></i>
                </span>
                <span class="hide-menu">Laporan</span>
              </a>
              
            </li>
            <!-- <li class="sidebar-item">
              <a class="sidebar-link" href="http://localhost/trueprojek/web/index.php?r=history%2Findex" aria-expanded="false">
                <span>
                  <i class="fa-solid fa-clock-rotate-left"></i>
                </span>
                <span class="hide-menu">History</span>
              </a>
            </li> -->
            <li class="nav-small-cap">
              <i class="ti ti-dots nav-small-cap-icon fs-4"></i>
              <span class="hide-menu">login</span>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link" href="http://localhost/trueprojek/web/index.php?r=site%2Flogin" aria-expanded="false">
                <span>
                  <i class="ti ti-login"></i>
                </span>
                <span class="hide-menu">Login</span>
              </a>
            </li>
            <li class="sidebar-item">
              <a class="sidebar-link" href="./authentication-register.html" aria-expanded="false">
                <span>
                  <i class="ti ti-user-plus"></i>
                </span>
                <span class="hide-menu">Register</span>
              </a>
            </li>
          </ul>
        </nav>
        <!-- End Sidebar navigation -->
      </div>
      <!-- End Sidebar scroll-->
    </aside>
    <!--  Sidebar End -->