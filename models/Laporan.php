<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "laporan".
 *
 * @property int $nisn
 * @property string $nama
 * @property string $kelas
 * @property string $jumlah_tunggakan
 * @property string $nominal_tunggakan
 */
class Laporan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'laporan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nisn', 'nama', 'kelas', 'jumlah_tunggakan', 'nominal_tunggakan'], 'required'],
            [['nisn'], 'integer'],
            [['nama'], 'string', 'max' => 55],
            [['kelas'], 'string', 'max' => 50],
            [['jumlah_tunggakan', 'nominal_tunggakan'], 'string', 'max' => 100],
            [['nisn'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nisn' => 'Nisn',
            'nama' => 'Nama',
            'kelas' => 'Kelas',
            'jumlah_tunggakan' => 'Jumlah Tunggakan',
            'nominal_tunggakan' => 'Nominal Tunggakan',
        ];
    }
}
