<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "transaksi".
 *
 * @property int $id_transaksi
 * @property int $id_petugas
 * @property string $nisn
 * @property string $tagihan_siswa
 * @property string $jumlah_bayar
 * @property string $kekurangan
 * @property string $tgl_bayar
 * @property int $id_spp
 * @property string $status
 */
class Transaksi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaksi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_petugas', 'nisn', 'tagihan_siswa', 'jumlah_bayar', 'kekurangan', 'tgl_bayar', 'id_spp', 'status'], 'required'],
            [['id_petugas', 'id_spp'], 'integer'],
            [['tgl_bayar'], 'safe'],
            [['status'], 'string'],
            [['nisn'], 'string', 'max' => 10],
            [['tagihan_siswa', 'jumlah_bayar', 'kekurangan'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_transaksi' => 'Id Transaksi',
            'id_petugas' => 'Id Petugas',
            'nisn' => 'Nisn',
            'tagihan_siswa' => 'Tagihan Siswa',
            'jumlah_bayar' => 'Jumlah Bayar',
            'kekurangan' => 'Kekurangan',
            'tgl_bayar' => 'Tgl Bayar',
            'id_spp' => 'Id Spp',
            'status' => 'Status',
        ];
    }
}
