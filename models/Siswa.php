<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "siswa".
 *
 * @property string $nisn
 * @property string $nis
 * @property string $nama
 * @property int $id_kelas
 * @property string $alamat
 * @property string $no_tlp
 * @property int $id_spp
 * @property string $tagihan
 */
class Siswa extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'siswa';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nisn', 'nis', 'nama', 'id_kelas', 'alamat', 'no_tlp', 'id_spp', 'tagihan'], 'required'],
            [['id_kelas', 'id_spp'], 'integer'],
            [['alamat'], 'string'],
            [['nisn'], 'string', 'max' => 10],
            [['nis'], 'string', 'max' => 8],
            [['nama'], 'string', 'max' => 35],
            [['no_tlp'], 'string', 'max' => 13],
            [['tagihan'], 'string', 'max' => 30],
            [['nisn'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nisn' => 'Nisn',
            'nis' => 'Nis',
            'nama' => 'Nama',
            'id_kelas' => 'Id Kelas',
            'alamat' => 'Alamat',
            'no_tlp' => 'No Tlp',
            'id_spp' => 'Id Spp',
            'tagihan' => 'Tagihan',
        ];
    }
}
