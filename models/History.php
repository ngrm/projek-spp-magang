<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "history".
 *
 * @property int $nisn
 * @property string $nama
 * @property int $id_kelas
 * @property int $id_spp
 * @property int $id_petugas
 * @property string $nominal
 * @property string $jumlah_dibayar
 * @property string $tanggal_dibayar
 * @property string $keterangan
 */
class History extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'history';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'id_kelas', 'id_spp', 'id_petugas', 'nominal', 'jumlah_dibayar', 'tanggal_dibayar', 'keterangan'], 'required'],
            [['id_kelas', 'id_spp', 'id_petugas'], 'integer'],
            [['tanggal_dibayar'], 'safe'],
            [['keterangan'], 'string'],
            [['nama'], 'string', 'max' => 35],
            [['nominal', 'jumlah_dibayar'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'nisn' => 'Nisn',
            'nama' => 'Nama',
            'id_kelas' => 'Id Kelas',
            'id_spp' => 'Id Spp',
            'id_petugas' => 'Id Petugas',
            'nominal' => 'Nominal',
            'jumlah_dibayar' => 'Jumlah Dibayar',
            'tanggal_dibayar' => 'Tanggal Dibayar',
            'keterangan' => 'Keterangan',
        ];
    }
}
