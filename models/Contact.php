<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact".
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $subject
 * @property string $body
 * @property int $agree
 */
class Contact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'email', 'subject', 'body', 'agree'], 'required'],
            [['subject', 'body'], 'string'],
            [['agree'], 'integer'],
            [['name'], 'string', 'max' => 35],
            [['email'], 'string', 'max' => 100],
            [['agree'], 'required', 'requiredValue' => 1, 'message' => 'You must agree to the terms.']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'email' => 'Email',
            'subject' => 'Subject',
            'body' => 'Body',
            'agree' => 'Agree',
        ];
    }
}
